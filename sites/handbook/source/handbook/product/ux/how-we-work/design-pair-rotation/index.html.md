---
layout: handbook-page-toc
title: "Product Design Pairs"
description: "Product designer pairs rotation schedule"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Design Pairs

This is the rotation schedule for FY24-Q3 and Q4 (2023-08-01 until 2024-01-31). Learn more about [Pair Designing](/handbook/product/ux/how-we-work/#pair-designing).

[//]: # TIP: To update the table below, create the schedule in a temporary spreadsheet, and then copy/paste the rows into an online markdown generator (https://www.google.com/search?q=copy-table-in-excel-and-paste-as-a-markdown-table)

| Designer               | Design Pair                |
|------------------------|----------------------------|
| Katie Macoy	         | Alex Fracazo               | 
| Mike Nichols	         | Camellia Yang              |
| Nick Brandt            | Jeremy Elder               |
| Michael Le             | Libor Vanc                 |
| Julia Miocene          | Gina Doyle	              |
| Veethika Mishra        | Annabel Gray               |
| Emily Bauman           | Pedro Moreira da Silva     |
| Austin Regnery         | Sunjung Park	              |
| Becka Lippert          | Matt Nearents              |
| Amelia Bauerly         | Emily Sybrant	          |
| Timothy Noah           | Dan Mizz-Harris, Sascha Eggenberger |
| Nick Leonard           | Michael Fangman            |        

### Temporary pairing

No temporary pairings at this time.
